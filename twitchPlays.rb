# Makes the first call into the dll to setup Twitch Plays integration
def enableTwitch?()
    fun = Win32API.new('TwitchPlays.dll', 'setup', %(l),'i')
    hWnd = Win32API.pbFindRgssWindow
    fun.call(hWnd) == 1
end


# Defines whether or not Twitch intergration is enabled
TWITCH_PLAYS = enableTwitch?()

# Wrapper for calling getNext from the dll
def getNext()
    fun = Win32API.new('TwitchPlays.dll', 'next', %(),'i')
    fun.call()
end

$CURRENT_KEY = -1
$PREVIOUS_KEY = -1

module Input
    class << self
        # Aliasing the old commands lets us use them if we want rather than losing them
        alias oldUpdate update
        alias oldPress? press?
        alias oldTrigger? trigger?
        alias oldRepeat? repeat?
        alias oldDir4 dir4
        alias oldDir8 dir8

        # By redefining each input function, we can change how it works.
        def update()
            if TWITCH_PLAYS
				$PREVIOUS_KEY = $CURRENT_KEY
                $CURRENT_KEY = getNext()
            else
                oldUpdate
            end
        end

        def press?(index)
            if TWITCH_PLAYS
                $CURRENT_KEY == index
            else
                oldPress?(index)
            end
        end

        def trigger?(index)
            if TWITCH_PLAYS
                # We only trigger on "KeyUp" so when the key changes that's our "KeyUp"
                $CURRENT_KEY != index && $PREVIOUS_KEY == index 
            else
                oldTrigger?(index)
            end
        end

        def repeat?(index)
            if TWITCH_PLAYS
                # This is counterintutive, but we DO NOT want commands to repeat because if we do, we get fast scrolling in menus
                $CURRENT_KEY == index && $CURRENT_KEY != $PREVIOUS_KEY
            else
                oldRepeat?(index)
            end
        end

        def dir4
            if TWITCH_PLAYS
				if [Input::UP, Input::DOWN, Input::LEFT, Input::RIGHT].include? $CURRENT_KEY
					$CURRENT_KEY
				else	
					0
				end
            else
                oldDir4
            end
        end

        def dir8
            if TWITCH_PLAYS
                # Since we don't have diagonal movement, we can just default to dir4
                dir4
            else
                oldDir8
            end
        end
    end
end
# Twitch Plays Integration for RPG Maker XP
This is a ruby file and dll for letting Twitch chat control any RPG Maker game with **VERY LITTLE** work from the original developer, while
allowing the Streamer to have as much control over how their sessions go as possible. See below to see how to use the TwitchPlays library
for your use case.

## I am a RPG Maker Developer who wants to support a TwitchPlays mode
All you need to do is create a new script in the script editor with the name "TwitchPlays" and the contents:
``` ruby
if File.exists?("#{Dir.getwd}/twitchPlays.rb")
    require "#{Dir.getwd}/twitchPlays.rb"
end
```
And you're done! Now users can add their own versions of the TwitchPlays.rb and TwitchPlays.dll to support how they want to handle their
stream. If you're feeling generous, you can always give the streamer a cheat sheet of variables that may be of interest for the overlay
(Such as map or chapter names.)

## I am a Streamer who wants to stream a TwitchPlays ready RPG Maker XP game
Download the latest release of the TwitchPlays tools [here](http://www.mediafire.com/file/j8ozd6c7rlczd5z/twitchPlays.zip), and extract them next to the "Game.exe" of the RPG Maker XP game
you wish to play. Then create a "twitchConfig.txt" file with the following values, each on a new line:
``` plaintext
[STREAM CHANNEL YOU WISH TO STREAM FROM]
[THE USERNAME OF YOUR BOT]
[THE OAUTH TOKEN (MINUS oauth:) FOR YOUR BOT FROM https://twitchapps.com/tmi/]
```
Now when you click "Game.exe" you should get a prompt asking if you want to enable Twitch Plays integration. Click yes, and the bot should
connect to chat automatically.

## I am a Ruby/C++ Developer who wants to make this tool better
You'll need Visual Studio 2017 (Community is freely available [here](https://www.visualstudio.com/downloads/)) with MFC and C++ tools
installed. After that you can clone this repo, and hack to your hearts content. I'm always accepting pull requests!

### Building
To build the TwitchPlays library, select "Release" and "x86" then press ctrl+shift+b.

### Testing
The easiest way to test your integration is to copy an appropriate RPG Maker XP project, and the twitchPlays.rb into the "Release" folder
generated in the root directory by the above build step, then run the game.

# License
Until I get a response on the licensing for PhraseMan's library, this project is 
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/). If PhraseMan
has a different license requirement, the license on this will be updated accordingly.

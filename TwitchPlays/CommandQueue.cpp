#include "stdafx.h"
#include "CommandQueue.h"


CommandQueue::CommandQueue()
{
	_head = NULL;
	_tail = NULL;
}


CommandQueue::~CommandQueue()
{
	while (_head != NULL) {
		Node* temp = _head->next;
		free(_head);
		_head = temp;
	}
}

void CommandQueue::Add(int value) {
	Node *n = (Node*)malloc(sizeof(Node));
	n->next = NULL;
	n->value = value;
	if (_head == NULL) {
		_head = n;
		_tail = n;
	}
	else {
		_tail->next = n;
	}
	_length++;
}

int CommandQueue::Get() {
	if (_head == NULL) {
		return -1;
	}
	Node* n = _head;
	_head = _head->next;
	int ret = n->value;
	free(n);
	_length--;
	return ret;
}

int CommandQueue::Length() {
	return _length;
}
#include "stdafx.h"
#include "Processors.h"

AnarchyProcessor::AnarchyProcessor() {
	commands = new CommandQueue();
}

void AnarchyProcessor::AddValue(int value) {
	commands->Add(value);
}

int AnarchyProcessor::NextValue() {
	return commands->Get();
}
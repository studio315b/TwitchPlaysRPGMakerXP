//////////////////////////////////////////////
// LIBRARY HEADER FOR Twitch-Library by PhraseMan
// All work (C) PhraseMan 2017
// Awaiting license information
// See: https://github.com/PhraseMan/Twitch-Library/issues/1
//////////////////////////////////////////////
#pragma once
#ifndef TWITCH_API_H
#define TWITCH_API_H

#include <basetsd.h>
#include <functional>
#include <string>
#include <vector>

typedef UINT_PTR socket_t;

typedef std::string string_t;
typedef std::wstring wstring_t;

namespace Twitch {
namespace V5 {

struct DateStruct {
  int year;
  int month;
  int day;
  int hour;
  int minute;
};

struct Channel {
  string_t broadcaster_language;
  string_t created_at;
  string_t streamer_url;
  string_t streamer_game;
  string_t streamer_logo;

  int32_t view_count;
  int32_t followers;

  bool partner;
  bool mature;
};

struct Follower {
  string_t updated_at;
  string_t display_name;
  string_t user_name;
  string_t user_id;
  string_t created_at;
};

struct Subscriber {
  string_t subscriber_name;
  string_t subscriber_plan;
  string_t created_at;
};
/**
 * \brief - Main interface to twitch api calls. ( Static Methods Do Not Require
 * An OAuth Key/Client ID )
 */
class TwitchApi {
 public:
  enum ClientCredentials { kClientId, kOAuthKey };

  TwitchApi(const string_t& client_id, const string_t& o_auth_key);
  ~TwitchApi();

  /**
   * \brief - Convert username to user id.
   */
  string_t UsernameToClientId(const string_t& username);
  /**
   * \brief - Get the last 25 subscribers of a channel, subscriber notifications
   * etc. (Needs OAuth Verification) \param channel_id - Channel ID used to
   * indentify the channel. \return -
   */
  std::vector<Subscriber> GetChannelSubscribers(const string_t& channel_id);

  /**
   * \brief - Valid commerical lengths.
   */
  enum CommericalLength {
    kThirtySeconds = 30,
    kMinute = 60,
    kMinuteAndAHalf = 90,
    kTwoMinutes = 120,
    kTwoMinutesAndAHalf = 150,
    kThreeMinutes = 180
  };
  /**
   * \brief - Starts a commerical on the channel id, must be the channel that
   * uses your OAuth token.
   * \param commerical_length - All valid commerical
   * lengths are listed in the enum.
   * \param channel_id - Channel Id, see:
   * 'UsernameToClientId' to convert channel name to id.
   * \return -
   */
  void StartChannelCommerical(const CommericalLength commerical_length,
                              const string_t& channel_id);

  /**
   * \brief - Get the last 25 followers of a channel, used for follower
   * notifications and etc. (Does not need OAuth Verification)
   * \param channel_id - Channel ID used to identify the channel.
   * \return - Container of the last
   * 25 followers of the channel.
   */
  std::vector<Follower> GetChannelFollowers(const string_t& channel_id,
                                            int32_t* follower_count = nullptr);

  /**
   * \brief - Get some useful data about a channel.
   * \param channel_id - Used to identify the stream, use 'UsernameToClientId'
   * if you want to use a username instead.
   * \return - A 'Channel' struct
   * containing all useful data from the api request.
   */
  Channel GetChannelDataById(const std::string& channel_id);

  string_t GetChannelUptime(const std::string& channel_name);

 private:
 protected:
  string_t client_id_;
  string_t oauth_token_;
};

}  // namespace V5
namespace Irc {

struct MessageData {
  string_t parsed_message;
  string_t parsed_username;
  string_t original_message;
  int32_t wsa_error_code;
};

class TwitchIrc {
 public:
  /**
   * \param oauth_token - OAuth Token generated here:
   * https://twitchapps.com/tmi/
   */
  explicit TwitchIrc(const string_t& oauth_token);
  ~TwitchIrc();

  /**
   * \brief - Connects to the Twitch IRC and recieves messages from the channel
   * you provide. Call MessageLoop() to recieve chat messages.
   * \param channel_name - Twitch channel name.
   * \param twitch_username - Your twitch username.
   * recieved by the IRC. Passed as username/message.
   */
  bool Connect(const string_t& channel_name, const string_t& twitch_username);

  /**
   * \brief - Send message to chat.
   * \param message - Chat message.
   * \param channel_name - The channel to send the message to, do not fill in
   * 'channel_name' if you want to use the channel name you used to connect to.
   */
  bool SendChatMessage(const string_t& message,
                       const string_t& channel_name = "");

  /**
   * \brief - Provides an interface to use the output from the chat.
   * \param message_callback - Message callback for username/message, provides a
   * TwitchIrc object to send chat messages once you receive a message and etc.
   */
  void MessageLoop(
      std::function<void(const string_t& user_name, const string_t& message,
                         TwitchIrc* twitch_irc)>
          message_callback);

  /**
   * \brief - An alternative way of getting chat messages if you do not want to
   * use the message loop.
   * \example: MessageData data = {0};
   * while(GetChatMessage(&data)) { std::cout << data.parsed_message <<
   * std::endl; } else { std::cout << data.wsa_error_code << std::endl }
   * \param - message_data - A struct you pass in to receive data from the call.
   * \return- true/false whether it got the message or not.
   *
   */
  bool GetChatMessage(MessageData* message_data);

  /**
   * \brief Stops the IRC from running.
   */
  void Disconnect();

 private:
  string_t oauth_token_;
  string_t channel_name_;
  socket_t my_socket_;
  bool connected_;

 protected:
};
namespace Parser {
/**
 * \brief - Provided Basic IRC Parser for username/message parsing. Make your
 * own if you want to something more complicated.
 */
class IrcParser {
 public:
  explicit IrcParser(const string_t& message);
  ~IrcParser() = default;
  string_t GetStrippedMessage() { return stripped_message_; };
  string_t GetUsername() { return username_; };
  int32_t GetType() { return type_; }

 private:
 protected:
  int32_t type_;
  string_t stripped_message_;
  string_t username_;
};
}  // namespace Parser
}  // namespace Irc

}  // namespace Twitch

namespace Crypto {
constexpr unsigned int SimpleHash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (SimpleHash(str, h + 1) * 33) ^ str[h];
}
}  // namespace Crypto

#endif
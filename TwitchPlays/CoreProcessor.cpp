#include "stdafx.h"
#include "PublicEndpoints.h"
#include "Processors.h"
#include "TwitchApi.h"
#include <algorithm>

ManualProcessor* manProc = new ManualProcessor();
Processor* active = new AnarchyProcessor();

const wchar_t *Get_WC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

/*
 * This is where inputs are converted to commands for the game, this should only ever process inputs to the game, never commands like 
 * "anarchy" "democracy" or other bot commands
 */
bool isInput(std::string input) {
	// the inputs from chat still have the trailing "\r\n" so we need to strip those
	input.erase(std::remove(input.begin(), input.end(), '\r'), input.end());
	input.erase(std::remove(input.begin(), input.end(), '\n'), input.end());
	// Hashing is faster than string comparisons
	switch (Crypto::SimpleHash(input.c_str()))
	{
	case Crypto::SimpleHash("start"):
		active->AddValue(INPUT_B);
		break;
	case Crypto::SimpleHash("b"):
		active->AddValue(INPUT_A);
		break;
	case Crypto::SimpleHash("a"):
		active->AddValue(INPUT_C);
		break;
	case Crypto::SimpleHash("up"):
		active->AddValue(INPUT_UP);
		break;
	case Crypto::SimpleHash("down"):
		active->AddValue(INPUT_DOWN);
		break;
	case Crypto::SimpleHash("left"):
		active->AddValue(INPUT_LEFT);
		break;
	case Crypto::SimpleHash("right"):
		active->AddValue(INPUT_RIGHT);
		break;
	default:
		return false;
		break;
	}
	return true;

}

/*
 * This is the root parser for chat commands, the first thing we do is check if it's an input command, if not, then handle other cases.
 * Cases should be their own function that returns a boolean if they succeed in handling the input and be called the same way as input.
 */
void HandleInput(std::string input) {
	if (isInput(input)) return;
	// Adding anarchy/democracy handling would go here
}

// Manual input is for the backup controller, just in case things go wrong
void ManualInput(int input) {
	manProc->AddValue(input);
}

// A wrapper to get the next command to be passed back to RPG Maker
int GetNextCommand() {
	if (manProc->IsActive()) {
		return manProc->NextValue();
	}
	return active->NextValue();
}

// TwitchPlays.h : main header file for the TwitchPlays DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTwitchPlaysApp
// See TwitchPlays.cpp for the implementation of this class
//

class CTwitchPlaysApp : public CWinApp
{
public:
	CTwitchPlaysApp();

	~CTwitchPlaysApp();

// Overrides
public:
	virtual BOOL InitInstance();

};

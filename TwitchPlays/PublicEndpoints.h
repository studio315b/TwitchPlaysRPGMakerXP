#pragma once
#include "stdafx.h"

//////////////////////////////////////////////
// CONSTANTS
//////////////////////////////////////////////

// Down 
#define INPUT_DOWN 2

// Left
#define INPUT_LEFT 4

// Right
#define INPUT_RIGHT 6

// Up
#define INPUT_UP 8

// z
#define INPUT_A 11

// Esc/X
#define INPUT_B 12

// Enter/C
#define INPUT_C 13
#define INPUT_X 14
#define INPUT_Y 15
#define INPUT_Z 16
#define INPUT_L 17
#define INPUT_R 18
#define INPUT_SHIFT 21
#define INPUT_CTRL 22
#define INPUT_ALT 23

// F
#define INPUT_F5 25
#define INPUT_F6 26
#define INPUT_F7 27
#define INPUT_F8 28
#define INPUT_F9 29

#define DLL_EXPORT __declspec(dllexport) 

//////////////////////////////////////////////
// Exported functions
//////////////////////////////////////////////

extern "C" DLL_EXPORT int setup(HWND);
extern "C" DLL_EXPORT int next();
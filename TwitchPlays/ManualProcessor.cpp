#include "stdafx.h"
#include "Processors.h"

ManualProcessor::ManualProcessor() {
}

int ManualProcessor::NextValue() {
	int ret = _currentInput;
	_currentInput = -1;
	return ret;
}

void ManualProcessor::AddValue(int input) {
	_currentInput = input;
}

bool ManualProcessor::IsActive() {
	return _currentInput != -1;
}
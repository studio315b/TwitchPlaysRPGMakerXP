// TwitchPlays.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "TwitchApi.h"
#include "TwitchPlays.h"
#include "PublicEndpoints.h"
#include "Processors.h"
#include <fstream>
#include <string>
#include <iostream>
#include <memory>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace Twitch::Irc;
using namespace Twitch::V5;
HANDLE thread;

// Dialog window for manual control
class myWindow : public CDialog {
public:
	myWindow() {
		Create(IDD_DIALOG1, NULL);
	}
	void OnCClicked() {
		ManualInput(INPUT_C);
	}
	void OnXClicked() {
		ManualInput(INPUT_B);
	}
	void OnLClicked() {
		ManualInput(INPUT_LEFT);
	}
	void OnRClicked() {
		ManualInput(INPUT_RIGHT);
	}
	void OnUClicked() {
		ManualInput(INPUT_UP);
	}
	void OnDClicked() {
		ManualInput(INPUT_DOWN);
	}
	DECLARE_MESSAGE_MAP()
};

// Input map
BEGIN_MESSAGE_MAP(myWindow, CDialog)
	ON_BN_CLICKED(IDC_C, &myWindow::OnCClicked)
	ON_BN_CLICKED(IDC_X, &myWindow::OnXClicked)
	ON_BN_CLICKED(IDC_D, &myWindow::OnDClicked)
	ON_BN_CLICKED(IDC_U, &myWindow::OnUClicked)
	ON_BN_CLICKED(IDC_L, &myWindow::OnLClicked)
	ON_BN_CLICKED(IDC_R, &myWindow::OnRClicked)
END_MESSAGE_MAP()

CTwitchPlaysApp::CTwitchPlaysApp()
{
}

CTwitchPlaysApp::~CTwitchPlaysApp() {
	TerminateThread(thread, 0);
}

CTwitchPlaysApp theApp;
myWindow *wnd;

// Create the main window for use later
BOOL CTwitchPlaysApp::InitInstance()
{
	CWinApp::InitInstance();

	wnd = new myWindow();
	m_pMainWnd = wnd;

	return TRUE;
}

// helper for failure messages
void failure(LPCWSTR reason) {
	MessageBox(NULL, reason, L"Setup Failure", MB_OK);
}

std::string channel, user, token;

// converts a string to correct format for MessageBox
const wchar_t *GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

// Reads the twitchConfig.txt file for connection information
// Expected format is:
// channel
// username
// token (minus ouath:)
bool readConfig() {
	try {
		std::ifstream infile("./twitchConfig.txt");
		if (infile) {
			getline(infile, channel);
			getline(infile, user);
			getline(infile, token);
		}
		infile.close();
		return true;
	}
	catch (...) {
		return false;
	}
}

// Message handler for TwitchChat
void HandleMessage(std::string user, std::string msg, TwitchIrc* irc) {
	HandleInput(msg.c_str());
}

// Thead function to processes chat messages
DWORD WINAPI BotThread( LPVOID lpParam) {
	auto twitch_irc = std::make_unique<TwitchIrc>(token);
	if (twitch_irc->Connect(channel, user)) {
		twitch_irc->MessageLoop(HandleMessage);
	}
	else {
		failure(L"Failed to connect to twitch");
	}
	return 0;
}

int holdCounter = HOLD_TIME;
int currentInput = -1;

// gets the next input, each input is "held" for HOLD_TIME frames so that movement works
DLL_EXPORT int next() {
	if (--holdCounter < 0) {
		currentInput = GetNextCommand();
		holdCounter = HOLD_TIME;
	}
	return currentInput;
}

// Checks if the user wants to enable twitch integration, then opens the manual control window if YES
// Returns the users choice to manipulate the Input module
DLL_EXPORT int setup(HWND hWnd) {
	if (MessageBox(hWnd, L"Do you want to enable Twitch Plays integration?", L"Twitch Plays Integration", MB_YESNO) == IDYES) {
		if (readConfig()) {
			wnd->ShowWindow(SW_SHOW);
			thread = CreateThread(NULL, 0, BotThread, NULL, 0, NULL);
			return 1;
		}
		else {
			failure(L"Failed to read config");
		}
	}
	return 0;
}

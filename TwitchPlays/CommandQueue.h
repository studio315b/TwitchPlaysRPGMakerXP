#pragma once

/*
 * This is a simple node structures for the command queue linked list. In theory we could add capabilities to store the user who input the
 * command or other data
 */
struct Node {
	// The input value
	int value;
	// The next node in the list
	Node* next;
};

// A Linked List backed data structure for managing a stream of inputs
class CommandQueue
{
public:
	CommandQueue();
	~CommandQueue();
	// Adds an input to the end of the queue
	void Add(int value);
	// Removes the first input in the queue and returns it's value
	int Get();
	// The length of the queue
	int Length();
private:
	Node* _head;
	Node* _tail;
	int _length;
};


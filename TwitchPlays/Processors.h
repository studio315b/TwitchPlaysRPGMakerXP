#pragma once
#include "CommandQueue.h"
#include <string>

#define HOLD_TIME 5;

void HandleInput(std::string);
void ManualInput(int);
int GetNextCommand();

/*
 * Each processor defined should implement the processor class so that they can be used interchangably by HandleInput and GetNextCommand
 */
class Processor
{
public:
	// Gets the next input value to be used
	virtual int NextValue() = 0;
	// Adds an input value for consideration
	virtual void AddValue(int) = 0;
};

/*
 * The processor for handling manual inputs (overrides all other processors)
 */
class ManualProcessor : public Processor {
public:
	ManualProcessor();
	virtual int NextValue();
	virtual void AddValue(int);
	bool IsActive();
private:
	int _currentInput;
};

/*
 * The processor for handling "Anarchy Mode" or original TPP input processing.
 */
class AnarchyProcessor : public Processor {
public:
	AnarchyProcessor();
	virtual int NextValue();
	virtual void AddValue(int);
private:
	CommandQueue* commands;
};